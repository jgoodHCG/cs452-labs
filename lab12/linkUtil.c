#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char* argv[])
{

	if(argc < 2) {
		printf("Usage: [options] fileName linkName\n");
		exit(1);
	}

	if(strtok(argv[1],"-s"))
	{
		if(link(argv[1],argv[2]) == -1){
			perror("Error creating hard link");
			exit(1);	
		}
	}
	else
	{
		if(symlink(argv[2],argv[3]) == -1){
			perror("Error creating soft link");
			exit(1);
		}
	}
	return 0;
}
