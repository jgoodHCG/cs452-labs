#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>

void sigHandler (int sigNum, siginfo_t *siginfo, void *context);

pid_t pid, ppid;

int main()
{
	struct sigaction sa;
	sa.sa_sigaction = &sigHandler;
	sa.sa_flags = SA_SIGINFO; 
	ppid = getpid();
	if((pid = fork()) < 0 )
	{
		printf("Fork failed!\n");
		exit(0);
	}
	if(pid) //parent code
	{
		while(1)
		{
			sigaction(SIGINT,&sa,NULL);
			sigaction(SIGUSR1,&sa,NULL);
			sigaction(SIGUSR2,&sa,NULL);

			printf("waiting...       ");
			fflush(stdout); 
			pause();
		}
	}
	if(!pid) 
	{
		while(1)
		{

        
			int a = rand() % 2;
			int r = rand() % 5;
			sleep(r);
			if(a == 0)
				kill(ppid,SIGUSR1);	//kill() actually sends the signal
			if(a == 1)
				kill(ppid,SIGUSR2);
		}
	}
return 0;
}
	
void  
sigHandler (int sigNum, siginfo_t *siginfo, void *context)
{
	if(sigNum == SIGUSR1)
	{
		printf("recieved a SIGUSR1 signal from PID: %d\n",siginfo->si_pid); 
		fflush(stdout);
	}
	if(sigNum == SIGUSR2)
	{
		printf("recieved a SIGUSR2 signal from PID: %d\n",siginfo->si_pid);
	}
	if(sigNum == SIGINT)
	{
		printf(" recieved. Closing program. \n");
		exit(0);
	}
}


