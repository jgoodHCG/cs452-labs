#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>

void sigHandler (int);

pid_t pid, ppid;

int main()
{
	ppid = getpid();
	if((pid = fork()) < 0 )
	{
		printf("Fork failed!\n");
		exit(0);
	}
	if(pid) //parent code
	{
		while(1)
		{
			signal(SIGINT,sigHandler);
			signal(SIGUSR1,sigHandler);
			signal(SIGUSR2,sigHandler);

			printf("waiting...       ");
			fflush(stdout);
			pause();
		}
	}
	if(!pid) //child code
	{
		while(1)
		{
			int a = rand() % 2;
			int r = rand() % 5;
			sleep(r);
			if(a == 0)
				kill(ppid,SIGUSR1);	//kill() actually sends the signal
			if(a == 1)
				kill(ppid,SIGUSR2);
		}
	}
return 0;
}
	
void
sigHandler (int sigNum)
{
	if(sigNum == SIGUSR1)
	{
		printf("recieved a SIGUSR1 signal\n");
		fflush(stdout);
	}
	if(sigNum == SIGUSR2)
	{
		printf("recieved a SIGUSR2 signal\n");
	}
	if(sigNum == SIGINT)
	{
		printf(" recieved. Closing program. \n");
		exit(0);
	}
}


