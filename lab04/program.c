#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

char *readline(void);
void* sim_fileaccess(void* arg);
int filesaccessed = 0;
void sigHandler (int);
int accFileAccessTime = 0;
int rc;

int main()
{
    
	char* string;
	pthread_t child;
	int status;

	while(1)
	{
        signal (SIGINT, sigHandler);
		printf("Please enter a file name: ");
		string = readline();
		if((status = pthread_create(&child, NULL, sim_fileaccess, string)) != 0)
		{
			fprintf(stderr, "Error creating the child thread %d: %s\n", status, strerror(status));
			exit(1);
		}
        if ((rc = pthread_detach(child)) != 0)
        {
            fprintf(stderr, "Error detaching the child thread %d: %s\n", rc , strerror(rc));
        }
    //	sleep(1);    //delete this line later
	}
	return 0;
}

void sigHandler (int sigNum)
{
    printf ("\nTotal files accessed: %d, \nAVG file access time: %d\n",filesaccessed,accFileAccessTime);
    
    sleep(1);

    exit(0);
}
char * readline(void)
{
	char* line = malloc(10);
	char* lineptr = line;
	int lenmax = 10;
	int len = lenmax;
   	int c;

    if(line == NULL)
        return NULL;

    while(1) {
       
	 c = fgetc(stdin);
        if(c == EOF){break;}

        if(--len == 0) {
            len = lenmax;
            char * linen = realloc(lineptr, lenmax *= 2);

            if(linen == NULL) {
                free(lineptr);
                return NULL;
            }
            line = linen + (line - lineptr);
            lineptr = linen;
        }

        if((*line++ = c) == '\n'){break;}
    }
    *line = '\0';
    return lineptr;
}
void* sim_fileaccess(void* arg)
{
	int i = rand() % 10;
	int stime = rand() % 3;
    accFileAccessTime = stime+7;    
	char *string = (char *) arg;
	if(i <= 2 && i >= 0){sleep(1);}
	else{sleep(stime+7);}
	printf("\nAccessed file named: %s\n",string);
	filesaccessed++;
    free(arg);
    return 0;
}

