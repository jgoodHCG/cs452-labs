#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>

int main()
{
   DIR *dirPtr;
   struct dirent *entryPtr;

   dirPtr = opendir (".");
    char *curFilename;
    struct stat statbuff; // TODO needs malloc??? or is that taken care of by stat?

   while ((entryPtr = readdir (dirPtr))){
      
      printf ("%-20s", entryPtr->d_name);
      
      curFilename = malloc(entryPtr->d_reclen);
      curFilename = entryPtr->d_name;
      
      if (stat (curFilename, &statbuff) < 0){
        perror("error calling stat");
        exit(1);
      }else{
        printf("    size: %llu\n", (unsigned long long)statbuff.st_size); 
      }

   }

   closedir (dirPtr);
   return 0;
} 
