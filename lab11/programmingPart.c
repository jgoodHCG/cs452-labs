#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[])
{
    struct stat statBuf;

    // supports none or three arguments
    // -s : size
    // -i : inode #
    // -F : filetype

    if (argc < 2){
	printf("Usage: %s <flag><file path>\n",argv[0]);
        exit(1);
    }
    if (argc == 3){
       if(strcmp(argv[1],"-s") == 0)
       {
            char *curFilename;
	        char *newFilename;

            struct dirent *entryPtr;
            // grab file stat
            if (stat (argv[2], &statBuf) < 0) {
                perror ("Error calling stat\n");
                exit(1);
            }

            // if path is a directory
            if (S_ISDIR(statBuf.st_mode)){
                DIR *dirPtr;
                unsigned long long totalsize;
                totalsize = 0;
                // print everythign within directory
                dirPtr = opendir (argv[2]);

                while ((entryPtr = readdir (dirPtr)))
                {
                    curFilename = entryPtr->d_name;
                    if(strcmp(curFilename,".") != 0 && strcmp(curFilename,"..") != 0)
                    {
                        newFilename = malloc(1024);
                        strcpy(newFilename, argv[2]);
                        strcat(newFilename,curFilename);

                        if (stat (newFilename, &statBuf) < 0){
                            perror("Error calling stat\n");
                            exit(1);
                        }else{
                            unsigned long long fileSize;
                            fileSize = (unsigned long long)statBuf.st_size;
                
                            int blocks = 1;

                            if(fileSize % 4096 != 0)
                            {
                                blocks = fileSize / 4096;
                                blocks++;
                                blocks *= 4;
                            }
                            else{
                                blocks = fileSize/4096;
                                blocks *=4;
                            }
                            printf( "%d %s\n", blocks,curFilename);
                            totalsize = totalsize + blocks;
                        }
		            }
                }
	        printf("Total %llu\n",totalsize);
	        free(newFilename);
            }
            else{ // file passed (not directory) for '-s'

                if (stat (argv[2], &statBuf) < 0){
                    perror("Error calling stat");
                    exit(1);
                }else{
                    unsigned long long fileSize;
                    fileSize = (unsigned long long)statBuf.st_size;
        
                    int blocks = 1;

                    if(fileSize % 4096 != 0)
                    {
                        blocks = fileSize / 4096;
                        blocks++;
                        blocks *= 4;
                    }
                    else{
                        blocks = fileSize/4096;
                        blocks *=4;
                    }
                    printf( "%d %s\n", blocks,argv[2]);
     
                }
            
            }    
        }       
       else if(strcmp( argv[1],"-i") == 0)
       {
	    char *newFilename;
            char *curFilename;
            struct dirent *entryPtr;
            // grab file stat
            if (stat (argv[2], &statBuf) < 0) {
                perror ("Error calling stat\n");
                exit(1);
            }
            // if path is a directory
            if (S_ISDIR(statBuf.st_mode)){
                DIR *dirPtr;
                unsigned long long totalsize;

                // print everythign within directory
                dirPtr = opendir (argv[2]);

                while ((entryPtr = readdir (dirPtr)))
                {
                  curFilename = entryPtr->d_name;
		  if(strcmp(curFilename,".") != 0 && strcmp(curFilename,"..") != 0)
		  {
			
			newFilename = malloc(1024);
			strcpy(newFilename, argv[2]);
		  	strcat(newFilename,curFilename);

			if(stat(newFilename,&statBuf) < 0) {
				perror("Error calling stat\n");
				exit(1);
			}
			printf("%ld %s\n",(long)statBuf.st_ino,curFilename);
		  }
		}
	}
	free(newFilename);
	}
       else if(strcmp(argv[1],"-F") == 0)
       {
	    char *newFilename;
            char *curFilename;
            struct dirent *entryPtr;
            // grab file stat
            if (stat (argv[2], &statBuf) < 0) {
                perror ("Error calling stat\n");
                exit(1);
            }
            // if path is a directory
            if (S_ISDIR(statBuf.st_mode)){
                DIR *dirPtr;
                unsigned long long totalsize;

                // print everythign within directory
                dirPtr = opendir (argv[2]);

                while ((entryPtr = readdir (dirPtr)))
                {
                  curFilename = entryPtr->d_name;
		  if(strcmp(curFilename,".") != 0 && strcmp(curFilename,"..") != 0)
		  {
			
			newFilename = malloc(1024);
			strcpy(newFilename, argv[2]);
		  	strcat(newFilename,curFilename);

			if(stat(newFilename,&statBuf) < 0) {
				perror("Error calling stat\n");
				exit(1);
			}

			switch(statBuf.st_mode & S_IFMT) {
			case S_IFREG:  printf("%s\n",curFilename);	 break;
			case S_IFDIR:  printf("%s/\n",curFilename);	 break;
			case S_IFLNK:  printf("%s@\n",curFilename);	 break;
			case S_IFSOCK: printf("%s=\n",curFilename);	 break;
			case S_IFIFO:  printf("%s|\n",curFilename);	 break;
			default: printf("Found unknown file type\n");    break;
			}

		  }
		}
	}
       }
}

	return 0;
}
