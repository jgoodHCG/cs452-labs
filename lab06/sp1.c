#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>

#define SIZE 16

int main (int argc, char* argv[])
{
   int status;
   long int i, loop, temp, *shmPtr;
   int shmId;
   pid_t pid;

  if(argc < 2)
  {
    printf("Usage: %s <loop number>\n",argv[0]);
    exit(0);
  }

  loop = atoi(argv[1]); // get value of loop variable (from command-line argument)

   if ((shmId = shmget (IPC_PRIVATE, SIZE, IPC_CREAT|S_IRUSR|S_IWUSR)) < 0) {
      perror ("i can't get no..\n");
      exit (1);
   }
   if ((shmPtr = shmat (shmId, 0, 0)) == (void*) -1) { //shmPtr is the address of the shared memory segment
      perror ("can't attach\n");
      exit (1);
   }

   shmPtr[0] = 0;
   shmPtr[1] = 1;

   if (!(pid = fork())) { //if child process
      for (i=0; i<loop; i++) {
      /* swap the contents of shmPtr[0] and shmPtr[1] */
      temp = shmPtr[0];
      shmPtr[0] = shmPtr[1];
      shmPtr[1] = temp;
  }
      if (shmdt (shmPtr) < 0) { //detach shared memory segment from child process
         perror ("just can't let go\n");
         exit (1);
      }
      exit(0); //exit child process
   }
   else //if parent process
      for (i=0; i<loop; i++) {
      /* swap the contents of shmPtr[1] and shmPtr[0] */
      temp = shmPtr [1];
      shmPtr[1] = shmPtr[0];
      shmPtr[0] = temp;
 }

   wait (&status); //parent waits until child terminates
   printf ("values: %li\t%li\n", shmPtr[0], shmPtr[1]);

   if (shmdt (shmPtr) < 0) { //detach shared memory segment from parent process
      perror ("just can't let go\n");
      exit (1);
   }
   if (shmctl (shmId, IPC_RMID, 0) < 0) { //mark the segment to be destroyed
      perror ("can't deallocate\n");
      exit(1);
   }

   return 0;
} 
