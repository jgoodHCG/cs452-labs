#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/sem.h> //include some semaphores

#define SIZE 16

int main (int argc, char* argv[])
{
   int status;
   long int i, loop, temp, *shmPtr;
   int shmId, semId;
   pid_t pid;

  /* Create a struct array that has two spots for operations *
   * increment and decrement or wait.                        */
  struct sembuf sops[2];

  /* Set up the structures */
  sops[0].sem_num = 0;
  sops[0].sem_op = -1; //decrement or wait
  sops[0].sem_flg = 0; //no flags

  sops[1].sem_num = 0;
  sops[1].sem_op = 1; //increment
  sops[1].sem_flg = 0; //no flags

  semId = semget(IPC_PRIVATE, 1, 00600); //create a semephore
  semctl (semId, 0 , SETVAL, 1); //set the value of the semaphore to 1

  if(argc < 2)
  {
    printf("Usage: %s <loop number>\n",argv[0]);
    exit(0);
  }

  loop = atoi(argv[1]); // get value of loop variable (from command-line argument)


   if ((shmId = shmget (IPC_PRIVATE, SIZE, IPC_CREAT|S_IRUSR|S_IWUSR)) < 0) {
      perror ("i can't get no..\n");
      exit (1);
   }
   if ((shmPtr = shmat (shmId, 0, 0)) == (void*) -1) { //shmPtr is the address of the shared memory segment
      perror ("can't attach\n");
      exit (1);
   }

   shmPtr[0] = 0;
   shmPtr[1] = 1;
   if (!(pid = fork())) { //if child process
      for (i=0; i<loop; i++) {

      /* Decrement semaphore if possible, otherwise sleep*/
      if(semop(semId, &sops[0], 1) == -1){printf("Error decrementing in child\n");}

      /* swap the contents of shmPtr[0] and shmPtr[1] */
      temp = shmPtr[0];
      shmPtr[0] = shmPtr[1];
      shmPtr[1] = temp;

     /* Increment semaphore */
     if(semop(semId, &sops[1], 1) == -1){printf("Error incrementing in child\n");}
  }
      if (shmdt (shmPtr) < 0) { //detach shared memory segment from child process
         perror ("just can't let go\n");
         exit (1);
      }
      exit(0); //exit child process
   }
   else //if parent process
      for (i=0; i<loop; i++) {

      /* Decrement semaphore if possible, otherwise sleep*/
      if(semop(semId, &sops[0], 1) == -1){printf("Error decrementing in parent\n");}

      /* swap the contents of shmPtr[1] and shmPtr[0] */
      temp = shmPtr [1];
      shmPtr[1] = shmPtr[0];
      shmPtr[0] = temp;

      /* Increment semaphore */
      if(semop(semId, &sops[1], 1) == -1){printf("Error incrementing in parent\n");}
 }

   wait (&status); //parent waits until child terminates
   printf ("values: %li\t%li\n", shmPtr[0], shmPtr[1]);


   semctl (semId, 0 , IPC_RMID, 1);
   if (shmdt (shmPtr) < 0) { //detach shared memory segment from parent process
      perror ("just can't let go\n");
      exit (1);
   }
   if (shmctl (shmId, IPC_RMID, 0) < 0) { //mark the segment to be destroyed
      perror ("can't deallocate\n");
      exit(1);
   }

   return 0;
} 
