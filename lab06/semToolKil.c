#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/sem.h>


struct sem {
    int semId:
    struct sembuf sops[2];
};

int create()
{
    struct sem semaphore;
    int shmId, semId;
    semaphore.semId = semget(IPC_PRIVATE, 1, 0600);
    return semaphore;
}

void initialize(sem s)
{
  s.sops[0].sem_num = 0;
  s.sops[0].sem_op = -1;
  s.sops[0].sem_flg = 0;
  
  s.sops[1].sem_num = 0;
  s.sops[1].sem_op =  1;
  s.sops[1].sem_flg = 0;
}

void wait (sem s)
{
// wait
    if(semop(s.semID, s.sops[0], 1) == -1){printf("Error decrementing.")}
// signal
    if(semop(semID, s.sops[1], 1) == -1){printf("Error ")}
}
// destroy
//  don't forget to destroy in the main program
