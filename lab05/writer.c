#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/shm.h> 
#include <unistd.h>

// create space + give read/write access
int main ()
{
   struct shmid_ds shmid_ds;
    int shmId;
   char *data;
    key_t key;
    struct message {
    int numPrinted;
    char* string;
    } ;

    int unlock;
    key = ftok("home/goodju/CS452/cs452labs/lab5/access",'R');
   if ((shmId = shmget (key, 1024, IPC_CREAT|S_IRUSR|S_IWUSR)) < 0) {
         perror ("i can't get no..\n");
         exit (1);
      }
   if ((data = shmat (shmId, 0, 0)) == (void*) -1) {
         perror ("can't attach\n");
         exit (1);
      }

// install signal handler

// make struct
char* s = "Whatever";
struct message  a1 = {0,s};
data = a1; // does this copy message to data and now there are two separate things?

// loop forever
    while(1)
    {
        // set  lock
        unlock = 0;
        // input string
        printf("\n>>> ");
        gets(data->string); // store string in mem segment
        data->numPrinted = 0; // set the amount that has printed this string to 0
        // loop until unclock
        while(!unlock)
        {
            // get stats
            if(shmctl(shmId,SHM_STAT,&shmid_ds)<0);
                {
                    perror("failed to access stats");
                }
            // if number processes that printed = to num of processes connected -writer
            if((shmid_ds.shm_nattch-1)==data->numPrinted)
            {
                unlock = 1;
            }
        }
    }
}
