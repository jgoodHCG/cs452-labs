#include <stdlib.h>
#include <stdio.h>

//data section
int glob1 = 10;
int glob2 = 10;

int main(int argc, char* argv[])
{
	printf("address of glob1 in data section %p\n",&glob1);
	printf("address of glob2 in data section %p\n",&glob2);

//stack
	int i = 10;
	printf("address of i on stack: %p\n",&i);
	int o = 10;
	printf("address of o on stack: %p\n",&o);

//heap
	void* p = malloc(10);
	printf("address of p on heap: %p\n",p);
	void* z = malloc(10);
	printf("address of z on heap: %p\n",z);

return 0;
}
